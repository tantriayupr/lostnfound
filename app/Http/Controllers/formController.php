<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IsianController extends Controller
{
    public function welcome(){
		return view('welcome');
    }
    
    public function description(){
        return view('description');
    }

    public function personal(){
        return view('personal');
    }

    public function confirmation(){
        return view('confirmation');
    }
}
