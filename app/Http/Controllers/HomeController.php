<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Dekripsi;
use App\City;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function index2()
    {
        $city = City::all();
        return view('generate',['city'=> $city]);
    }

    public function filter() {
        $data = ['date', 'origin', 'destination', 'flightnumber'];
        $range = ['from', 'to'];

        $collection = [];
        $used_var = [];
        $r = [];
        $used_r = [];

        for($i=0; $i < count($range); $i++) {
            if(!empty(request($range[$i]))){
                $r[$range[$i]] = request($range[$i]);
                $used_r[$range[$i]] = request($range[$i]);
            }
            else{
                $r[$range[$i]] = 'False';
            }
        }

        // get data yang disubmit user sesuai dengan var $data
        for ($i=0; $i < count($data); $i++) { 
            if(!empty(request($data[$i]))){
                $collection[$data[$i]] = request($data[$i]);
                $used_var[$data[$i]] = request($data[$i]);
            }
            else{
                $collection[$data[$i]] = 'False';
            }
        }

        $data = collect([
            (object) $collection,
            (object) $r
        ]);

        $city = City::all();
        
        // return data kalau field date diisi
        if(!empty(request('date'))) {
            $filter = DB::table('dekripsis')->where('date', request('date'))->get();

            return view('generate', ['filter' => $filter, 'city'=> $city, 'data' => $data]);
        }
        else if(!empty(request('flightnumber'))) {
            $filter = DB::table('dekripsis')->where('flightnumber', request('flightnumber'))->get();

            return view('generate', ['filter' => $filter, 'city'=> $city, 'data' => $data]);
        }
        else if(!empty(request('from')) && !empty(request('to'))) {
            $filter = DB::table('dekripsis')->where($used_var)
                        ->whereBetween('date', [request('from'), request('to')])->get();

            return view('generate', ['filter' => $filter, 'city'=> $city, 'data' => $data]);
        }
        else{
            $filter = DB::table('dekripsis')->where($used_var)
                        ->where('date', $used_r)->get();

            return view('generate', ['filter' => $filter, 'city'=> $city, 'data' => $data]);
        }
 
    }

    public function report(){
        return view('genreport');
    }
    
    public function showFunction(){
        $dekripsi = DB::table('dekripsi')->get();
        return view('');
    }

    function excel($from, $to, $origin, $destination, $date, $flightnumber)
    {

        if($date != 'False'){
            $data =  DB::table('dekripsis')->where('date', $date)->get()->toArray();
        }
        else if($flightnumber != 'False'){
            $data =  DB::table('dekripsis')->where('flightnumber', $flightnumber)->get()->toArray();
        }
        // if($from != NULL) {
        //     $data =  DB::table('dekripsis')->where($collection)
        //                 ->whereBetween('date', [$range['from'], $range['to']])->get()->toArray();
        // }
        
        $data_array[] = array('Date of loss', 'Location', 'Location Details', 'Flight Number', 'Origin','Destination','Type of Property','Item Characteristics','Picture of Item','Name','Email','Phone Number');
        foreach($data as $p)
        {
         $data_array[] = array(
          'Date of loss'            => $p->date,
          'Location'                => $p->location,
          'Location Details'        => $p->locationdetails,
          'Flight Number'           => $p->flightnumber,
          'Origin'                  => $p->origin,
          'Destination'             => $p->destination,
          'Type of Property'        => $p->typeproperty,
          'Item Characteristics'    => $p->other,
          'Picture of Item'         => $p->itemimage,
          'Name'                    => $p->name,
          'Email'                   => $p->email,
          'Phone Number'            => $p->phone

         );
        }
        Excel::create('dekripsis', function($excel) use ($data_array){
            $excel->setTitle('dekripsis');
            $excel->sheet('dekripsis', function($sheet) use ($data_array){
             $sheet->fromArray($data_array, null, 'A1', false, false);
            });
        })->download('xlsx');
    }
    


    
}
