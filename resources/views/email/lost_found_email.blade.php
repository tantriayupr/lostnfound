<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="x-apple-disable-message-reformatting">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <title></title>
    <!--[if (mso 16)]>
    <style type="text/css">
    a {text-decoration: none;}
    </style>
    <![endif]-->
    <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]-->
    <!--[if !mso]><!-- -->
    <link href="https://fonts.googleapis.com/css?family=Open Sans:400,400i,700,700i" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <!--<![endif]-->
</head>

<body>
    <div class="es-wrapper-color">
        <!--[if gte mso 9]>
			<v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
				<v:fill type="tile" color="#eeeeee"></v:fill>
			</v:background>
		<![endif]-->
        <table width="100%" class="es-wrapper" cellspacing="0" cellpadding="0">
            <tbody>
                <tr>
                    <td class="esd-email-paddings" valign="top">
                        <table align="center" class="es-content esd-header-popover" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                </tr>
                                <tr>
                                    <td align="center" class="esd-stripe" esd-custom-block-id="7954">
                                        <table width="600" align="center" class="es-content-body" style="background-color: transparent;" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td align="left" class="esd-structure es-p15t es-p15b es-p10r es-p10l">
                                                        <!--[if mso]><table width="580" cellpadding="0" cellspacing="0"><tr><td width="282" valign="top"><![endif]-->
                                                        <table align="left" class="es-left" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="282" align="left" class="esd-container-frame">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td align="left" class="es-infoblock esd-block-text es-m-txt-c">
                                                                                        <p style="font-family: arial, helvetica\ neue, helvetica, sans-serif;"><br></p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <!--[if mso]></td><td width="20"></td><td width="278" valign="top"><![endif]-->
                                                        <table align="right" class="es-right" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="278" align="left" class="esd-container-frame">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td align="right" class="es-infoblock esd-block-text es-m-txt-c">
                                                                                        <p><br></p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <!--[if mso]></td></tr></table><![endif]-->
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table align="center" class="es-content" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                </tr>
                                <tr>
                                    <td align="center" class="esd-stripe" esd-custom-block-id="7681">
                                        <table width="600" align="center" class="es-header-body" style="background-color: rgb(4, 71, 103);" bgcolor="#044767" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td align="left" class="esd-structure es-p35t es-p35b es-p35r es-p35l">
                                                        <!--[if mso]><table width="530" cellpadding="0" cellspacing="0"><tr><td width="340" valign="top"><![endif]-->
                                                        <table align="left" class="es-left" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="340" align="center" class="es-m-p0r es-m-p20b esd-container-frame" valign="top">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td align="center" class="esd-block-text es-m-txt-c">
                                                                                        <h1 style="color: #ffffff; line-height: 100%;">Lost & Found</h1>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <!--[if mso]></td><td width="20"></td><td width="170" valign="top"><![endif]-->
                                                        <table align="right" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr class="es-hidden">
                                                                    <td width="170" align="left" class="es-m-p20b esd-container-frame" esd-custom-block-id="7704">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td align="center" class="esd-block-spacer es-p5b">
                                                                                        <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td style="border-bottom: 1px solid rgb(4, 71, 103); background: rgba(0, 0, 0, 0) none repeat scroll 0% 0%; height: 1px; width: 100%; margin: 0px;"></td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table align="right" cellspacing="0" cellpadding="0">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td align="left">
                                                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <td align="right" class="esd-block-text">
                                                                                                                        <p><br></p>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                    <td align="left" class="esd-block-image es-p10l" valign="top">
                                                                                                        <a href target="_blank"><img width="50" style="display: block;" alt src="https://demo.stripocdn.email/content/guids/29bd5fc5-7751-4665-ac63-589164a44052/images/90721563290262540.png"></a> </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <!--[if mso]></td></tr></table><![endif]-->
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table align="center" class="es-content" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td align="center" class="esd-stripe">
                                        <table width="600" align="center" class="es-content-body" bgcolor="#ffffff" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td align="left" class="esd-structure es-p40t es-p35b es-p35r es-p35l" style="background-color: rgb(247, 247, 247);" bgcolor="#f7f7f7" esd-custom-block-id="7685">
                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="530" align="center" class="esd-container-frame" valign="top">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td align="center" class="esd-block-image es-p20t es-p25b es-p35r es-p35l">
                                                                                        <a href="https://viewstripo.email/" target="_blank"> <img width="100%" title="ship" style="display: block;" alt="ship" src="https://demo.stripocdn.email/content/guids/29bd5fc5-7751-4665-ac63-589164a44052/images/31211563282918872.jpg"> </a> </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="center" class="esd-block-text es-p15b">
                                                                                        <h2 style="color: #333333; font-family: 'open sans', 'helvetica neue', helvetica, arial, sans-serif;"><br></h2>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="left" class="esd-block-text es-m-txt-l es-p20t">
                                                                                        <h3 style="font-size: 18px;">Hello , {{ $data->name }} </h3>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="left" class="esd-block-text es-p15t es-p10b">
                                                                                        <p style="font-size: 16px; color: #777777;">Thank you for filling in the lost items form. Here are some details from your report .<br></p>
                                                                                        <p style="font-size: 16px; color: #777777;"><br></p>
                                                                                        <p style="font-size: 16px; color: #777777;">Your details:<br></p>
                                                                                        <table style="width:100%" class="table table-striped table-bordered table-hover table-condensed">
  
                                                                                            <tr>
                                                                                                <th>Name:</th>
                                                                                                <td>{{ $data->name }}</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <th>Email:</th>
                                                                                                <td>{{ $data->email }}</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <th>Phone Number:</th>
                                                                                                <td>{{ $data->phone }}</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <th>Location:</th>
                                                                                                <td>{{ $data->location }}</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <th>Location Details:</th>
                                                                                                <td>{{ $data->locationdetails }}</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <th>Origin:</th>
                                                                                                <td>{{ $data->origin }}</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <th>Destination:</th>
                                                                                                <td>{{ $data->destination }}</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <th>Property:</th>
                                                                                                <td>{{ $data->typeproperty }}</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <th>Others:</th>
                                                                                                <td>{{ $data->other }}</td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <th>Image:</th>
                                                                                                <td>{{ $data->itemimage }}</td>
                                                                                            </tr>
                                                                                            </table>
                                                                                        <p style="font-size: 16px; color: #777777;"><br></p>
                                                                                        <p style="font-size: 16px; color: #777777;">We will try our best to keep you informed, please kindly wait up to 3 days of work for further informations.<br></p>
                                                                                        <p style="font-size: 16px; color: #777777;">Thank you<br></p>
                                                                                        <br>
                                                                                        <p style="font-size: 16px; color: #777777;">If you didn't expect this email, then you can safely ignore it.</p>
                                                                                    </td>
                                                                                </tr>
                                                                                <br>
                                                                                <br>
                                                                                <br>
                                                                                <tr>
                                                                                    <td align="center" class="esd-block-button es-p25t es-p20b es-p10r es-p10l">
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table align="center" class="es-content" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                </tr>
                                <tr>
                                    <td align="center" class="esd-stripe" esd-custom-block-id="7797">
                                        <table width="600" align="center" class="es-content-body" style="background-color: rgb(27, 155, 163);" bgcolor="#1b9ba3" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td align="left" class="esd-structure es-p35t es-p35b es-p35r es-p35l">
                                                        <!--[if mso]><table width="530" cellpadding="0" cellspacing="0"><tr><td width="340" valign="top"><![endif]-->
                                                        <table align="left" class="es-left" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="340" align="center" class="es-m-p0r es-m-p20b esd-container-frame" valign="top">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td align="left" class="esd-block-text es-m-txt-c">
                                                                                        <h1 style="color: #ffffff; line-height: 100%;"><br></h1>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <!--[if mso]></td><td width="20"></td><td width="170" valign="top"><![endif]-->
                                                        <table align="right" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr class="es-hidden">
                                                                    <td width="170" align="left" class="es-m-p20b esd-container-frame" esd-custom-block-id="7704">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td>
                                                                                        <table align="right" cellspacing="0" cellpadding="0">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td align="left">
                                                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                                                            <tbody>
                                                                                                                <tr>
                                                                                                                    <td align="right" class="esd-block-text">
                                                                                                                        <p><br></p>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </tbody>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                    <td align="left" class="esd-block-image es-p10l" valign="top">
                                                                                                        <a href target="_blank"><img width="50" style="display: block;" alt src></a> </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <!--[if mso]></td></tr></table><![endif]-->
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table align="center" class="es-footer" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td align="center" class="esd-stripe" esd-custom-block-id="7684">
                                        <table width="600" align="center" class="es-footer-body" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td align="left" class="esd-structure es-p35t es-p40b es-p35r es-p35l">
                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="530" align="center" class="esd-container-frame" valign="top">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                            <tr>
                                                                            <td align="center" class="esd-block-text es-m-txt-c es-p5b" esdev-links-color="#777777">
                                                                                        <p style="color: #777777;"><br></p>
                                                                                        <p style="color: #777777;"></p>
                                                                                    </td>
                                                                                    </tr>


                                                                                <tr>
                                                                                    <td align="center" class="esd-block-image es-p15b">
                                                                                        <a target="_blank"> <img width="200" title="Beretun logo" style="display: block;" alt="Beretun logo" src="https://demo.stripocdn.email/content/guids/29bd5fc5-7751-4665-ac63-589164a44052/images/62851563285730400.png"> </a> </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="center" class="esd-block-text es-m-txt-c es-p5b" esdev-links-color="#777777">
                                                                                        <p style="color: #777777;"><br></p>
                                                                                        <p style="color: #777777;">© Garuda Indonesia 2019</p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table align="center" class="esd-footer-popover es-content" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td align="center" class="esd-stripe">
                                        <table width="600" align="center" class="es-content-body" style="background-color: transparent;" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td align="left" class="esd-structure es-p30t es-p30b es-p20r es-p20l">
                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="560" align="center" class="esd-container-frame" valign="top">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td align="center" class="esd-empty-container" style="display: none;"></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</body>

</html>