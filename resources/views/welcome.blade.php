<!DOCTYPE html>
<html lang="en">
<head>
  <title>Lost Property</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="text-center">
<ul class="pagination">
  <li class="active"><a href="/">Information</a></li>
  <li><a href="description">Description</a></li>
  <li><a href="personal">Personal Details</a></li>
  <li><a href="confirmation">Confirmation</a></li>
</ul>
</div>


<div class="container">
  <div class="jumbotron"> 
    <h1 align="center"></h1>      
    <p>We're sorry to hear that you lost something, but we'll do our best to check and restore it to you.</p>
    <p>We will ask you to describe the item, including where and when you lost it. That way, it will increase the chance of us finding it for you.</p>
    <p>Enquiries regarding items that has been lost for more than 90 days since the lost date will not be followed up.</p>  
     <ul class="pager">
    <li class="previous"><a href="#">Cancel</a></li>
    <li class="next"><a href="description">Next</a></li>
  </ul> 
  </div>   
</div>

</body>
</html>
