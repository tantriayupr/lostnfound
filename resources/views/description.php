<!DOCTYPE html>
<html lang="en">
<head>
  <title>Lost Property</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script>
  $( function() {
    $( "#datepicker" ).datepicker({ minDate: -20, maxDate: "+1M +15D" });
  } );
  </script>
  </head><script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<body>

<div class="text-center">
<ul class="pagination">
  <li ><a href="/">Information</a></li>
  <li class="active"><a href="description">Description</a></li>
  <li><a href="personal">Personal Details</a></li>
  <li><a href="confirmation">Confirmation</a></li>
</ul>
</div>

<div class="container">
  <div class="jumbotron">   
  <h2 align="center">Describe your property</h2>
  <br>
    <form method="post" action="/description" enctype="multipart/form-data">

    <div class="form-group row">
            <label for="dateid" class="col-sm-3 col-form-label">Date of loss</label>
            <div class="col-sm-9">
            <input type="text" class="form-control" id="datepicker">
            </div>
        </div>

       

        <div class="form-group row">
            <label for="locationid" class="col-sm-3 col-form-label">Location</label>
            <div class="col-sm-9" >
            <form>
                    <div class="form-group">
                    <select class="form-control" id="sel1">
                        <option>Flight</option>
                        <option>Airport</option>
                    </select>
                    </div>
            </form>
            </div>
        </div>


        <div class="form-group row">
            <label for="locationdetailsid" class="col-sm-3 col-form-label">Location Details</label>
            <div class="col-sm-9">
                <input name="locationdetails" type="text" class="form-control" id="locationdetailsid"
                       placeholder="Location Details">
            </div>
        </div>

        <div class="form-group row">
            <label for="flightnumberid" class="col-sm-3 col-form-label">Flight Number</label>
            <div class="col-sm-9">
                <input name="flightnumber" type="text" class="form-control" id="flightnumberid"
                       placeholder="Flight Number">
            </div>
        </div>

        <div class="form-group row">
            <label for="originid" class="col-sm-3 col-form-label">Origin</label>
            <div class="col-sm-9">
                <input name="origin" type="text" class="form-control" id="originid"
                       placeholder="Origin">
            </div>
        </div>

        <div class="form-group row">
            <label for="destinationid" class="col-sm-3 col-form-label">Destination</label>
            <div class="col-sm-9">
                <input name="destination" type="text" class="form-control" id="destinationid"
                       placeholder="Destination">
            </div>
        </div>

        <div class="form-group row">
            <label for="typepropertyid" class="col-sm-3 col-form-label">Type of Property</label>
            <div class="col-sm-9" >
            <form>
                    <div class="form-group">
                    <select class="form-control" id="sel1">
                        <option>Agenda</option>
                        <option>Artwork</option>
                        <option>Audio</option>
                        <option>Book</option>
                        <option>Clothing</option>
                        <option>Computer</option>
                        <option>Currency</option>
                        <option>Document</option>
                        <option>Duty Free</option>
                        <option>Glasses</option>
                        <option>ID</option>
                        <option>Jewellery</option>
                        <option>Key</option>
                        <option>Luggage</option>
                        <option>Medication</option>
                        <option>Musical Instrument</option>
                        <option>Phone</option>
                        <option>Toys</option>
                        <option>Video</option>
                        <option>Wallet</option>
                        <option>Other</option>
                    </select>
                    </div>
            </form>
            </div>
        </div>

        <div class="form-group row">
            <label for="otherid" class="col-sm-3 col-form-label">Item Characteristics</label>
            <div class="col-sm-9">
                <input name="other" type="text" class="form-control" id="otherid"
                       placeholder="Please write down the item characteristics">
            </div>
        </div>
    
        <div class="form-group row">
            <label for="itemimageid" class="col-sm-3 col-form-label">Picture of Item</label>
            <div class="col-sm-9">
                <input name="itemimage" type="file" id="itemimageid" class="custom-file-input">
                <span style="margin-left: 15px; width: 480px;" class="custom-file-control"></span>
            </div>
        </div>
    </form>

     <ul class="pager">
    <li class="previous"><a href="/">Cancel</a></li>
    <li class="next"><a href="personal">Next</a></li>
  </ul> 
  </div>   
</div>

</body>
</html>
