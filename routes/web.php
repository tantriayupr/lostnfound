<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

// Route::get('/description', function(){
//     return view('description');
// });

// Route::get('/personal',function(){
//     return view('personal');
// });

// route::get('/confirmation', function(){
//     return view('confirmation');
// });
Route::get('/inf','DekripsiController@infFunction');
Route::get('/desc', 'DekripsiController@descFunction');
Route::post('/submit-desk', 'DekripsiController@storeDesc')->name('store.desc');
Route::get('/confirm', function() {
    return view('confirmation');
});  

Route::get('/kirimemail','DekripsiController@index');





Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/generate', 'HomeController@index2')->name('generate');

Route::get('/report', 'HomeController@report');

Route::get('/home-1', 'HomeController@showFunction');


Route::post('/filter', 'HomeController@filter')->name('filter');

Route::get('filter/to_excel/{from}/{to}/{origin}/{destination}/{date}/{flightnumber}', 'HomeController@excel')->name('to_excel');

Route::get('/export_excel', 'ExportExcelController@index3')->name('export_excel');
Route::get('/export_excel/excel', 'ExportExcelController@excel')->name('export_excel.excel');


